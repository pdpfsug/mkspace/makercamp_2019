# Presentazione
Da inserire

# Intro
Nel **MakerCamp 2019** si andra' a costruirure quasi titto il tempo con attivita di varia natura che comprende ma non si limita a: creazione della carta a mano, il suo utilizzio a livello artistico e tecnologico, robot e schede programmabili, utilizzo di linguaggi di programazione per la realizzaizone delle proprie idee, l'invenzione di un gioco da tavolo nel vero spirito maker.  
E' un evento indirizzato ai giovani dai 6 ai 25 anni.  
E' organizzato dall'associazione [PDPFSUG](https://pdp.linux.it/) presso il [MakerSpace](https://bibliomarchenord.it/SebinaOpac/article/makerspace/bf_makerspace?sysb=fabriano) della [Biblioteca Multimediale Fariano](https://bibliomarchenord.it/SebinaOpac/Opac.do?sysb=fabriano&).  
Il **MakerCamp 2019** si svolgera' tra il 9 e 13 Luglio.  

# Trivia
*  Durante il **MakerCamp** i partecipanti vengono chiamati **camper**.
*  **PDPFSUG** e' un'associazione che promuove il software libero.
*  **Makerspace** e' uno spazio per attivita making all'interno di una biblioteca pubblica.

# Attivita'
* [Allegro Chirurgo](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/allegro_chirurgo.txt)
* [Assemblaggio PC](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/assemblaggio_pc.txt)
* [Colora Papertronik](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/colora_papertronik.txt)
* [Crea Papertronik](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/crea_papertronik.txt)
* [Esegui Papertronik](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/esegui_papertronik.txt)
* [Gioco da Tavolo](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/gioco_da_tavolo.txt)
* [Microbit](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/microbit.txt)
* [Ritiro Papertronik](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/ritiro_papertronik.txt)
* [Scratch & Company](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/scratch_and_company.txt)
* [Chiusura](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/chiusura.txt)

# Data Svolgimento
* 9 Luglio - ITIS FABRIANO
    - 09:15
        + [Crea Papertronik](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/crea_papertronik.txt)
* 10 Luglio - MAKERSPACE BIBLIOTECA MULTIMEDIALE FABRIANO
    - 15:30
        + [Allegro Chirurgo](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/allegro_chirurgo.txt)
        + [Assemblaggio PC](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/assemblaggio_pc.txt)
        + [Gioco da Tavolo](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/gioco_da_tavolo.txt)
        + [Microbit](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/microbit.txt)
        + [Scratch & Company](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/scratch_and_company.txt)
    - 18:30
        + [Chiusura](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/chiusura.txt)
* 11 Luglio - MAKERSPACE BIBLIOTECA MULTIMEDIALE FABRIANO
    - 15:30
        + [Allegro Chirurgo](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/allegro_chirurgo.txt)
        + [Assemblaggio PC](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/assemblaggio_pc.txt)
        + [Gioco da Tavolo](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/gioco_da_tavolo.txt)
        + [Microbit](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/microbit.txt)
        + [Scratch & Company](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/scratch_and_company.txt)
    - 18:30
        + [Chiusura](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/chiusura.txt)
* 12 Luglio - MAKERSPACE BIBLIOTECA MULTIMEDIALE FABRIANO
    - 10:00
        + [Ritiro Papertronik](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/ritiro_papertronik.txt)
    - 15:30
        + [Colora Papertronik](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/colora_papertronik.txt)
        + [Esegui Papertronik](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/esegui_papertronik.txt)
    - 18:30
        + [Chiusura](https://gitlab.com/pdpfsug/mkspace/makercamp_2019/blob/master/attivita/chiusura.txt)