Info
----
Referente: Gursham
Tutor: Guramrit
Data: 10-12 Luglio

Introduzione
------------
Assemblare un PC da zero.

Materiali
---------
2 Computer con tutti i component. 


Descrizione
-----------
Prendi i case dei PC del Makerspace.
Smonta i pc, RAM, CPU, HDD ecc. e mettili sul tavolo.
Poi procedi all'assemblaggio.
Una volta assemblato, verifica che tutti i componenti siano stati correttamente fissati.
Ora ripeti il procedimento.